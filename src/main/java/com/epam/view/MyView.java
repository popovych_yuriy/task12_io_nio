package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Task2 - Set, print and serialize ship");
        menu.put("2", "  2 - Task2 - Deserialize and print ship");
        menu.put("3", "  3 - Task3 - Test reader: usual, buffered, buffered(1mb)");
        menu.put("4", "  4 - Task4 - Test your implementation of InputStream");
        menu.put("5", "  5 - Task5 - Read source code, print comments");
        menu.put("6", "  6 - Task6 - Show the contents of a specific directory(src)");
        menu.put("7", "  7 - Task7 - Run SomeBuffer");
        menu.put("8", "  8 - Task8 - Run Server");
        menu.put("9", "  9 - Task8 - Run Client");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::serializeShip);
        methodsMenu.put("2", this::deserializeShip);
        methodsMenu.put("3", this::testReader);
        methodsMenu.put("4", this::testInputStream);
        methodsMenu.put("5", this::readSourceCode);
        methodsMenu.put("6", this::showDirectory);
        methodsMenu.put("7", this::runSomeBuffer);
        methodsMenu.put("8", this::runServer);
        methodsMenu.put("9", this::runClient);
    }

    private void serializeShip() {
        logger.info("Ship: ");
        logger.info(controller.setShip());
        controller.serializeShip();
        logger.info("Ship with droids serialized in Ship.dat");
    }

    private void deserializeShip() {
        logger.info("Deserialized Ship: ");
        logger.info(controller.deserializeShip());
    }

    private void testReader() {
        try {
            controller.testReader();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showDirectory() {
        try {
            controller.showDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void testInputStream() {
        try {
            controller.testMyInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readSourceCode() {
        try {
            controller.readSourceCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runSomeBuffer() {
        controller.runSomeBuffer();
    }

    private void runServer() {
        controller.runServer();
    }

    private void runClient() {
        controller.runClient();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
