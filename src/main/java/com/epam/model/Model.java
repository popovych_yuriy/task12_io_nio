package com.epam.model;

import com.epam.model.shipdroid.Ship;

import java.io.IOException;

public interface Model {
    Ship setShip();

    void serializeShip();

    Ship deserializeShip();

    void testReader() throws IOException;

    void showDirectory() throws IOException;

    void testMyInputStream() throws IOException;

    void readSourceCode() throws IOException;

    void runSomeBuffer();

    void runServer();

    void runClient();
}
