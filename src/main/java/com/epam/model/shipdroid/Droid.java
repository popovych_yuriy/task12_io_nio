package com.epam.model.shipdroid;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int health;
    private int damage;
    private transient int id;

    public Droid(String name, int health, int damage, int id) {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", damage=" + damage +
                ", id=" + id +
                '}';
    }
}
