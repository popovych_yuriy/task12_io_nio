package com.epam.model.shipdroid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {
    private List<Droid> droids = new ArrayList<>();

    public List<Droid> getDroids() {
        return droids;
    }

    public void addDroid(Droid droid) {
        this.droids.add(droid);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "droids=" + droids +
                '}';
    }
}
