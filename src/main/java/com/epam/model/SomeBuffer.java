package com.epam.model;

import com.epam.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class SomeBuffer {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private String path;
    private FileChannel fileChanel;
    private ByteBuffer byteBuffer;

    public SomeBuffer(String path) {
        this.path = path;
    }

    public void readFromFile(){
        try {
            fileChanel = FileChannel.open(Paths.get(path));
            byteBuffer = ByteBuffer.allocate(1024);
            logger.info("Read from file(ForTask5.java): ");
            while (fileChanel.read(byteBuffer) != -1){
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()){
                    System.out.print((char)byteBuffer.get());
                }
                byteBuffer.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void writeToFIle(String line){
        try {
            fileChanel = FileChannel.open(Paths.get(path), StandardOpenOption.WRITE);
            byteBuffer = ByteBuffer.wrap(line.getBytes());
            fileChanel.write(byteBuffer);
            logger.info("Write to file: " + line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
