package com.epam.model.clientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {
    private static Logger logger = LogManager.getLogger(Server.class);

    @SuppressWarnings("unused")
    public void run() throws IOException {
        Selector selector = Selector.open();
        ServerSocketChannel crunchifySocket = ServerSocketChannel.open();
        InetSocketAddress crunchifyAddr = new InetSocketAddress("localhost", 1111);
        crunchifySocket.bind(crunchifyAddr);
        crunchifySocket.configureBlocking(false);
        int ops = crunchifySocket.validOps();
        SelectionKey selectKy = crunchifySocket.register(selector, ops, null);

        while (true) {
            logger.info("i'm a server and i'm waiting for new connection and buffer select...");
            selector.select();
            Set<SelectionKey> crunchifyKeys = selector.selectedKeys();
            Iterator<SelectionKey> crunchifyIterator = crunchifyKeys.iterator();

            while (crunchifyIterator.hasNext()) {
                SelectionKey myKey = crunchifyIterator.next();
                if (myKey.isAcceptable()) {
                    SocketChannel crunchifyClient = crunchifySocket.accept();
                    crunchifyClient.configureBlocking(false);
                    crunchifyClient.register(selector, SelectionKey.OP_READ);
                    logger.info("Connection Accepted: " + crunchifyClient.getLocalAddress() + "\n");
                } else if (myKey.isReadable()) {
                    SocketChannel crunchifyClient = (SocketChannel) myKey.channel();
                    ByteBuffer crunchifyBuffer = ByteBuffer.allocate(256);
                    crunchifyClient.read(crunchifyBuffer);
                    String result = new String(crunchifyBuffer.array()).trim();
                    logger.info("Message received: " + result);

                    if (result.equals("Crunchify")) {
                        crunchifyClient.close();
                        logger.info("\nIt's time to close connection as we got last company name 'Crunchify'");
                        logger.info("\nServer will keep running. Try running client again to establish new connection");
                    }
                }
                crunchifyIterator.remove();
            }
        }
    }
}
