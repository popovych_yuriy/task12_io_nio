package com.epam.model;

import com.epam.model.clientserver.Client;
import com.epam.model.clientserver.Server;
import com.epam.model.shipdroid.Droid;
import com.epam.model.shipdroid.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Scanner;

public class BusinessLogic implements Model {
    private static Logger logger = LogManager.getLogger(BusinessLogic.class);
    Ship ship;

    @Override
    public Ship setShip() {
        ship = new Ship();
        ship.addDroid(new Droid("С-3РО", 10, 1, 001));
        ship.addDroid(new Droid("R2D2", 20, 5, 002));
        ship.addDroid(new Droid("HK-47", 100, 100, 003));
        return ship;
    }

    @Override
    public void serializeShip() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream("Ship.dat"));
            out.writeObject(ship);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Ship deserializeShip() {
        try {
            FileInputStream fileIn = new FileInputStream("Ship.dat");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ship = (Ship) in.readObject();
            in.close();
            fileIn.close();
            return ship;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            logger.info("Ship class not found");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void testReader() throws IOException {
        long startTime;
        long endTime;
        long duration;

        logger.info("Test usual reader:");
        startTime = System.nanoTime();
        int count = 0;
        InputStream inputstream =
                new FileInputStream("task3.pptx");
        int data = inputstream.read();
        while (data != -1) {
            data = inputstream.read();
            count++;
        }
        inputstream.close();
        logger.info("count= " + count);
        endTime = System.nanoTime();
        duration = (endTime - startTime) / 1000000;
        logger.info("End test usual reader, it took " + duration + " milliseconds");

        logger.info("Test buffered reader:");
        startTime = System.nanoTime();
        count = 0;
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("task3.pptx")));
        try {
            while (true) {
                byte b = in.readByte();
                count++;
            }
        } catch (EOFException e) {
        }
        in.close();
        logger.info("count=" + count);
        endTime = System.nanoTime();
        duration = (endTime - startTime) / 1000000;
        logger.info("End test buffered reader, it took " + duration + " milliseconds");

        logger.info("Test reader with 1 MB buffer:");
        startTime = System.nanoTime();
        int bufferSize = 1 * 1024 * 1024;
        DataInputStream in2 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("task3.pptx"), bufferSize));
        count = 0;
        try {
            while (true) {
                byte b = in2.readByte();
                count++;
            }
        } catch (EOFException e) {
        }
        in.close();
        logger.info("count=" + count);
        endTime = System.nanoTime();
        duration = (endTime - startTime) / 1000000;
        logger.info("End test reader with 1 MB buffer, it took " + duration + " milliseconds");
    }

    @Override
    public void showDirectory() throws IOException {
        logger.info("Contents of directory:");
        File file = new File("src/");
        if (file.exists()) {
            printFile(file, "");
        } else logger.info("directory do not exists");
    }

    private void printFile(File file, String str) {
        logger.info(str + "Directory: " + file.getName());
        str = str + "  ";
        File[] fileNames = file.listFiles();
        for (File f : fileNames) {
            if (f.isDirectory()) {
                printFile(f, str);
            } else {
                System.out.println(str + "File: " + f.getName());
            }
        }
    }

    @Override
    public void testMyInputStream() throws IOException {
        InputStream inputStream = new FileInputStream("task4.txt");
        logger.info("Test MyInputStream:");
        try (MyInputStream stream = new MyInputStream(inputStream, 10)) {
            int count = 0;
            int data = stream.read();
            while (data != -1) {
                count++;
                byte aByte = (byte) data;
                data = stream.read();
            }
            logger.info("count= " + count);
        }
        inputStream.close();
        logger.info("End test MyInputStream reader");
    }

    @Override
    public void readSourceCode() throws IOException {
        logger.info("Print file name(ForTask5.java)");
        try (Scanner sc = new Scanner(System.in)) {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(sc.nextLine())));
            while (true) {
                String line = in.readLine();
                int index = line.indexOf("//");
                if (line == null) {
                    break;
                }
                if (index != -1) {
                    logger.info("Comments in the sourcecode: ");
                    logger.info(line.substring(index));
                }
            }
            in.close();
        } catch (FileNotFoundException e) {
            logger.info(e);
        }
    }

    @Override
    public void runSomeBuffer() {
        SomeBuffer buffer = new SomeBuffer("ForTask5.java");
        buffer.readFromFile();
        buffer.writeToFIle("Test task7 write to file.");
    }

    @Override
    public void runServer() {
        Server server = new Server();
        try {
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void runClient() {
        Client client = new Client();
        try {
            client.run();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

