package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;
import com.epam.model.shipdroid.Ship;

import java.io.IOException;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public Ship setShip() {
        return model.setShip();
    }

    @Override
    public void serializeShip() {
        model.serializeShip();
    }

    @Override
    public Ship deserializeShip() {
        return model.deserializeShip();
    }

    @Override
    public void testReader() throws IOException {
        model.testReader();
    }

    @Override
    public void showDirectory() throws IOException {
        model.showDirectory();
    }

    @Override
    public void testMyInputStream() throws IOException {
        model.testMyInputStream();
    }

    @Override
    public void readSourceCode() throws IOException {
        model.readSourceCode();
    }

    @Override
    public void runSomeBuffer() {
        model.runSomeBuffer();
    }

    @Override
    public void runServer() {
        model.runServer();
    }

    @Override
    public void runClient() {
        model.runClient();
    }
}